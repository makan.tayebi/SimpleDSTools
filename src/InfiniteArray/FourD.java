/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package InfiniteArray;

import java.awt.Point;

/**
 *
 * @author Makan&Armin
 */
public class FourD<T> {

    private int dLength = 65536; // 65536 is 2 ^ (64 / 4). Each dimention of array is 2^(64/4)
    private long d2 = 4294967296L; // 2 ^ (64 / 2)
    private long d3 = 281474976710656L; //2 ^ (64 * 3 / 4)
    private Object[][][][] arr = new Object[dLength][][][];
    private int first, second, third, forth;

    public void set(int i, T val) {
        first = (int) (i / d3);
        second = (int) ((i % d3) / d2);
        third = (int) ((i % d2) / dLength);
        forth = (int) (i % dLength);
        if (arr[first] == null) {
            arr[first] = new Object[dLength][][];
        }
        if (arr[first][second] == null) {
            arr[first][second] = new Object[dLength][];
        }
        if (arr[first][second][third] == null) {
            arr[first][second][third] = new Object[dLength];
        }
        arr[first][second][third][forth] = val;
    }
    /*public boolean delete(int i){
    }*/
    
    public T get(int i) {
        first = (int) (i / d3);
        second = (int) ((i % d3) / d2);
        third = (int) ((i % d2) / dLength);
        forth = (int) (i % dLength);
        try {
            return (T) arr[first][second][third][forth];
        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String args[]) {
        FourD<Point> anbaari = new FourD<Point>();
        anbaari.set(11, new Point(3, 3));
        System.out.println(anbaari.get(11));
    }
}
