package Train;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Makan
 */
public class ObjTrain<T> {

    Wagon first, last, head;
    private int wagonSize, wagons, trainSize;
    public int size;

    public ObjTrain(int wagonSize) {
        if (wagonSize < 1) {
            wagonSize = 1;
        }
        first = new Wagon<T>(wagonSize, 1);
        this.wagonSize = wagonSize;
        first.setPrev(null);
        first.setNext(last);
        head = first;
        last = first;
        wagons = 1;
        size = 0;
        trainSize = wagonSize;
    }

    public void headToStart() {
        head = first;
    }

    public void set(int i, T val) {
        if (i >= size) {
            size = i + 1;
        }
        while (i >= trainSize) {
            addWagon();
        }

        while ((i / wagonSize + 1) > head.getNumber()) {
            head = head.getNext();
        }

        while ((i / wagonSize + 1) < head.getNumber()) {
            head = head.getPrev();
        }
        // at this line, head is pointing to the right wagon.
        head.set(val, (i % wagonSize));
    }

    public T get(int i) {
        while (i >= trainSize) {
            addWagon();
        }
        while ((i / wagonSize + 1) > head.getNumber()) {
            head = head.getNext();
        }

        while ((i / wagonSize + 1) < head.getNumber()) {
            head = head.getPrev();
        }
        return (T) head.get(i % wagonSize);
    }

    public int getLength() {
        return size;
    }

    public void clear() {
        size = 0;
        head = first;
    }

    private void addWagon() {
        wagons++;
        trainSize += wagonSize;
        last.setNext(new Wagon<T>(wagonSize, wagons));
        last.getNext().setPrev(last);
        last = last.getNext();
    }

    public void cutOff(int i) {
        while (1 + (i / wagonSize) < wagons) {
            wagons--;
            last = last.getPrev();
        }
        last.setNext(null);
        if (size >= last.getNumber() * wagonSize) {
            size = last.getNumber() * wagonSize - 1;
        }
        head = first;
    }

    public static void main(String args[]) {
        ObjTrain<String> array = new ObjTrain<String>(10);
        array.set(17, "hey");
        System.out.println(array.get(17));
    }
}

class Wagon<T> {

    private int number;
    private Wagon<T> next, prev;
    private Object[] arr;

    public Wagon(int size, int number) {
        this.number = number;
        arr = new Object[size];
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public Wagon getPrev() {
        return prev;
    }

    public void setPrev(Wagon prev) {
        this.prev = prev;
    }

    public void setNext(Wagon next) {
        this.next = next;
    }

    public Wagon getNext() {
        return next;
    }

    public void set(T val, int index) {
        arr[index] = val;
    }

    public T get(int index) {
        return (T) arr[index];
    }
}